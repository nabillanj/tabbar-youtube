//
//  AppDelegate.swift
//  YoutubeExample
//
//  Created by Nabilla on 10/02/20.
//  Copyright © 2020 PT Intersolusi Teknologi Asia. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let layout = UICollectionViewFlowLayout()
        window?.rootViewController = UINavigationController(rootViewController: ViewController(collectionViewLayout: layout))
        
        let statusBackgroundView = UIView()
        statusBackgroundView.backgroundColor = UIColor.rgb(red: 230, green: 31, blue: 31)
        window?.addSubview(statusBackgroundView)
        
        window?.translatesAutoresizingMaskIntoConstraints = false
        window?.addConstraintWithFormat(format: "H:|[v0]|", views: statusBackgroundView)
        window?.addConstraintWithFormat(format: "V:|[v0(20)]", views: statusBackgroundView)
        
        return true
    }
}

