//
//  MenuCell.swift
//  YoutubeExample
//
//  Created by Nabilla on 11/02/20.
//  Copyright © 2020 PT Intersolusi Teknologi Asia. All rights reserved.
//

import UIKit

class MenuCell: BaseCell {
    
    let imageView: UIImageView = {
       var img = UIImageView()
        img.image = UIImage(named: "home")?.withRenderingMode(.alwaysTemplate)
        img.contentMode = .scaleAspectFill
        img.tintColor = UIColor.rgb(red: 91, green: 14, blue: 13)
        return img
    }()
    
    override var isHighlighted: Bool {
        didSet {
            imageView.tintColor = isHighlighted ? UIColor.white : UIColor.rgb(red: 91, green: 14, blue: 13)
        }
    }
    
    override var isSelected: Bool {
        didSet {
            imageView.tintColor = isSelected ? UIColor.white : UIColor.rgb(red: 91, green: 14, blue: 13)
        }
    }
    
    override func setupViews() {
        super.setupViews()
        addSubview(imageView)
        addConstraintWithFormat(format: "H:[v0(20)]", views: imageView)
        addConstraintWithFormat(format: "V:[v0(20)]", views: imageView)

        addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
}
